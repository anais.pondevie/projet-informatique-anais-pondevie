pyknon.genmidi import Midi

from pyknon.music import NoteSeq

import subprocess

#Questions Globales  # la répétition correspond a la bonne réponse

 

print(" Taper la bonne réponse A,B ou C ( ne pas oublier les majuscules , c'est une série de 10 questions . Si vous répondez autre chose que A B C le programme plantera il faudra le relancer . " )

 

 # Ecriture des questions et des réponses correspondantes

Question = [["Quelle musique est jouée par Bach ?","A :silence","B :canon cancrizans","C :a fleur de toi","B :canon cancrizans"],

            ["Qui a écrit: ne me quitte pas ? ","A :Jacques Brel","B :Renauld","C :Edit Piaf","A :Jacques Brel"],

            ["Quelle est la musique de Serge Gainsbourg ?","A :Je suis venu te dire que je m'en vais","B :La Corrida","C :La Vie en rose","A :Je suis venu te dire que je m'en vais"],

            ["Qu'est ce que Noir Désir ?","A :Un groupe de hard-rock","B :Un groupe de rap","C :Un groupe de rock","C :Un groupe de rock"],

            ["Qui a écrit Morgane de toi(Amoureux de toi) ?","A :Renault","B :Johnny Hallyday","C :Alain Delon","A :Renault"] ,

            ["Saint JHN a-t-il ecrit Roses ? ","A :Oui","B :Non","C :Je ne sais pas","A :Oui"],

            ["Qui a écrit la musique virale de la coupe du monde 2018 ?","A :Shakira","B :Magic Systeme","C :Vegedream","C :Vegedream"],

            ["Quelle est la musique top 2019 en France ?","A :Avant toi- Slimane","B :Médicament- Niska","C :Tombé- M;Pokora","B :Médicament- Niska"],

            ["Quelle est la dernière musiqe de soprano ? ","A :Roule ?","B :A la vie à l'amour ? ","C :A nos héros du quotidien ?","C :A nos héros du quotidien ?"],

            ["Pour quelle occasion Louane a-t-elle chantée Je Vole ? ","A :Pour son album","B :Pour un film","C :Sans raisons particulère","B :Pour un film"]]

              



# on écrit la musique finale

def Bach(sequenceDG: str = "c2' eb2 g2 ab2 b2, r4 g2' f#2 f2 e2 eb2 d4 db4 c4 b4, g4 c4' f4 eb2 d2 c2 eb2 g8 f8 g8 c8'' g8' eb8 d8 eb8 f8 g8 a8 b8 c8'' eb8' f8 g8 ab8 d8 eb8 f8 g8 f8 eb8 d8 eb8 f8 g8 ab8 bb8 ab8 g8 f8 g8 ab8 bb8 c8'' db8 bb8' ab8 g8 a8 b8 c8'' d8 eb8 c8 b8' a8 b8 c8'' d8 eb8 f8 d8 g8' d8'' c8 d8 eb8 f8 eb8 d8 c8 b8' c4'' g4' eb4 c4"):

    sequenceGD = sequenceDG.split()

    sequenceGD.reverse()

    sequenceGD = "".join(sequenceGD)



    sequenceDG = NoteSeq(sequenceDG)

    sequenceGD = NoteSeq(sequenceGD)



    midiSeq = Midi(2, tempo=80, instrument=0)



    midiSeq.seq_notes(sequenceDG, track=0)

    midiSeq.seq_notes(sequenceGD, track=1)



    midiSeq.write("bach.midi")

    subprocess.check_call(["timidity", "bach.midi"])





 

x = 0

pts = 0

while x < 10 :

    lQst = random.choice(Question)  #On prend une question

    Question.remove(lQst)  #Qu'on enleve de notre liste de questions

    print(lQst[0])  #On l'affiche

 

     

    print(lQst[1]) #Les 3 réponses

    print(lQst[2])

    print(lQst[3])





    lRep = input("Votre réponse: ") #On redemande

  

    if lRep == "A": #On récupère le choix

        lChx = lQst[1]

       

    elif lRep == "B":

        lChx = lQst[2]

    else :

        lChx = lQst[3]

  

    if lChx == lQst[4]: #rpvrai

        print("-- VRAI:) --")

        pts += 1

       

    else: #Sinon

        print("-- FAUX:( --")

        print(" La bonne réponse était : "+str(lQst[4]))

    x += 1



Bach()

print(" Fin du jeu ! Votre score : "+str(pts)+"/10")
